# Import the basic framework components.
from cothread.catools import caget, connect, camonitor,caput
from softioc import softioc, builder
import cothread
import Cpsc
import Npsc
import time

import pickle
import json
from os.path import exists
import pigpio

#Read from the save file:
file_path = 'autosave.json'
if exists(file_path):
    with open(file_path) as json_file:
        autosave = json.load(json_file)

# Or make a new one if we don't have any
else:
    autosave = {}

def update_autosave(pv_name, value):

    autosave[pv_name] = value
    with open(file_path, 'w') as fp:
        json.dump( autosave,fp)



# Set the record prefix
builder.SetDeviceName("SISSY2EX:HITS:MOT")
builder.SetBlocking(True)



#connect to the two devices
Cpsc = Cpsc.Cpsc('/dev/ttyUSB1')
Npsc = Npsc.Npsc('/dev/ttyUSB0')
cla_les_limit_pin = 24
cla_hes_limit_pin = 23

#create the PV's

class NPSC_AXIS():

    """
    npsc : Npsc connection
    prefix: "LES" or "HES"
    """

    def __init__(self, npsc, prefix,names,timeout=10, **kwargs):
        self.npsc = npsc
        self.prefix = prefix
        self.timeout = timeout


        # create the PV's

        
        self.pos_sp = builder.mbbOut(self.prefix+':POS-SP',names[0],names[1],names[2],names[3],names[4],initial_value=0)
        self.off_sp = builder.aOut(self.prefix+':OFF-SP',initial_value=0, DRVL=0, DRVH=1.0, EGU="mm")
        self.move = builder.boolOut(self.prefix+':MOVE', initial_value=0, always_update=True, on_update= lambda v: self.move_axis(v))
        
  
        self.pos_rb = builder.mbbIn(self.prefix+':POS-RB',names[0],names[1],names[2],names[3],names[4])
        self.off_rb = builder.aIn(self.prefix+':OFF-RB')
        self.done = builder.longIn(self.prefix+':DONE')

        self.error = builder.mbbIn(self.prefix+':STATUS',"No Error","Positive Limit Switch", "Negative Limit Switch", "Thermal Overload", "Motor Slipping")
        
        cothread.Spawn(self.update)

    def update(self):

        while True:
            if self.prefix == "LES":
                response = self.npsc.LesStatus()
            elif self.prefix == "HES":
                response = self.npsc.HesStatus()

            response = response.rstrip()
            #print(f"{self.prefix} response = {response}")  
            #print(self.npsc.axis_running)                   
            tokenList = response.split(",")                
            self.pos_rb.set(int(tokenList[0]))
            self.off_rb.set(float(tokenList[1]))
            self.done.set(int(tokenList[2]))
            self.error.set(int(tokenList[3]))
            cothread.Sleep(0.2)



    def move_axis(self,v):

        if v == 1:
            pos = self.pos_sp.get()
            off = self.off_sp.get()
            print(f"Moving to Pos {pos}, Off {off}")

            while bool(self.npsc.axis_running):

                #wait for the NPSC to stop moving the other axis if it is
                cothread.Sleep(1)

            #Take the lock
            self.npsc.axis_running = True

            if self.prefix == "LES":

                # write the values
                self.npsc.LesOff(pos,off)
            
            elif self.prefix == "HES":

                # write the values
                self.npsc.HesOff(pos,off)

            
            t0 = time.perf_counter()
            t1 = time.perf_counter()

            print(f"t0 = {t0}, t1={t1}, timeout = {float(self.timeout)}")
            self.done.set(0)


            while not bool(self.done.get()) and t1-t0<float(self.timeout):

                cothread.Sleep(1) 
                t1 = time.perf_counter()
                print(f"{self.prefix} moving done = {self.done.get()} t0 = {t0}, t1={t1}")

            #once complete this function will complete
            print("moved finished")
            self.npsc.axis_running = False


class CPSC_ES():

    """
    CPSC module for moving the HES and LES energy stages
    cpsc : Cpsc connection
    """

    def __init__(self, cpsc, timeout=10, **kwargs):
        self.cpsc = cpsc
        self.timeout = timeout

        # Instantiate the limit switches
        self.pi1 = pigpio.pi()
        self.pi1.set_pull_up_down(cla_hes_limit_pin, pigpio.PUD_DOWN)
        self.pi1.set_pull_up_down(cla_les_limit_pin, pigpio.PUD_DOWN)
        
        # create the PV's

        self.hes_dir_sp = builder.boolOut('CLA:HES:DIR-SP',ZNAM="RX+",ONAM="RX-",initial_value=0)
        self.hes_dur_sp = builder.longOut('CLA:HES:DUR-SP',DRVL=1, DRVH=20,initial_value=1, EGU="s")
        self.hes_move = builder.boolOut('CLA:HES:MOVE', initial_value=0, always_update=True,on_update_name= lambda v,name: self.move_axis(v,name))
        self.hes_pos_rb = builder.longOut('CLA:HES:POS-RB', initial_value=0, EGU="s")
        self.hes_limit_rb = builder.boolIn('CLA:HES:LIMIT-RB')
        self.hes_home = builder.boolOut('CLA:HES:HOME', initial_value=0, always_update=True,on_update_name= lambda v,name: self.home_axis(v,name))
        self.hes_stop = builder.boolOut('CLA:HES:STOP',ZNAM="STOP",ONAM="ENABLED", initial_value=0)
        self.hes_upper_limit = builder.longOut('CLA:HES:ULIM',DRVL=1,DRVH=90,initial_value=80, EGU="s")

        self.les_dir_sp = builder.boolOut('CLA:LES:DIR-SP',ZNAM="RX+",ONAM="RX-",initial_value=0)
        self.les_dur_sp = builder.longOut('CLA:LES:DUR-SP',DRVL=1, DRVH=20,initial_value=1, EGU="s")
        self.les_move = builder.boolOut('CLA:LES:MOVE', initial_value=0,always_update=True, on_update_name= lambda v,name: self.move_axis(v,name))
        self.les_pos_rb = builder.longOut('CLA:LES:POS-RB', initial_value=0, EGU="s")
        self.les_limit_rb = builder.boolIn('CLA:LES:LIMIT-RB')
        self.les_home = builder.boolOut('CLA:LES:HOME', initial_value=0, always_update=True,on_update_name= lambda v,name: self.home_axis(v,name))
        self.les_stop = builder.boolOut('CLA:LES:STOP',ZNAM="STOP",ONAM="ENABLED", initial_value=0)
        self.les_upper_limit = builder.longOut('CLA:LES:ULIM',DRVL=1,DRVH=90,initial_value=60, EGU="s")

        self.done = builder.longIn('CLA:ES:DONE')
        self.done.set(1)
        self.error = builder.longIn('CLA:ES:STATUS')
        self._done = 1
        cothread.Spawn(self.update)

    def update(self):

        while True:
          
            response = self.cpsc.ClaHesStatus().rstrip()

            #print(f"CLA ES response = {response}")  
            #print(self.cpsc.axis_running)                   
            tokenList = response.split(",")                
            
            self._done = int(tokenList[0])
            self.error.set(int(tokenList[1].lower(),16))

            #poll the limit switches
            self.les_limit_rb.set(not(bool(self.pi1.read(cla_les_limit_pin))))
            self.hes_limit_rb.set(not(bool(self.pi1.read(cla_hes_limit_pin))))

            cothread.Sleep(0.2)

    def home_axis(self,v, name):

        if v == 1:

            if "HES" in name:

                while bool(self.pi1.read(cla_hes_limit_pin)):

                    # While the limit switch is not engaged move in steps of 1 until it is

                    self.done.set(0)
                    self._done = 0

                    self.cpsc.ClaHesTime("RX-","1")
                    print("Moving CLA HES 1 second towards limit switch")
                    while not bool(self._done):
                        cothread.Sleep(0.05)

                print("HES Limit Switch engaged")

                while not bool(self.pi1.read(cla_hes_limit_pin)):

                    # While the limit switch is not engaged move in steps of 1 until it is

                    self._done = 0

                    self.cpsc.ClaHesTime("RX+","1")
                    print("Moving CLA HES 1 second away from limit switch")
                    while not bool(self._done):
                        cothread.Sleep(0.05)

                print("CLA HES axis is homed. Setting to 0")
                self.hes_pos_rb.set(0)
                self.done.set(1)

            if "LES" in name:

                while bool(self.pi1.read(cla_les_limit_pin)):

                    # While the limit switch is not engaged move in steps of 1 until it is
                    self.done.set(0)
                    self._done = 0

                    self.cpsc.ClaLesTime("RX-","1")
                    print("Moving CLA LES 1 second towards limit switch")
                    while not bool(self._done):
                        cothread.Sleep(0.05)

                print("LES Limit Switch engaged")

                while not bool(self.pi1.read(cla_les_limit_pin)):

                    # While the limit switch is not engaged move in steps of 1 until it is
                    self._done = 0
                    

                    self.cpsc.ClaLesTime("RX+","1")
                    print("Moving LES 1 second away from limit switch")
                    while not bool(self._done):
                        cothread.Sleep(0.05)

                print("CLA LES axis is homed. Setting to 0")
                self.les_pos_rb.set(0)
                self.done.set(1)


    def move_axis(self,v,name):

        if v == 1:

            if "HES" in name:

                if self.hes_dir_sp.get() == 0:

                    direction = "RX+"
                    dir_sign = 1
                
                elif self.hes_dir_sp.get() == 1:

                    direction = "RX-"
                    dir_sign = -1

                duration = self.hes_dur_sp.get()

            elif "LES" in name:

                if self.les_dir_sp.get() == 0:

                    direction = "RX+"
                    dir_sign = 1
                
                elif self.les_dir_sp.get() == 1:

                    direction = "RX-"
                    dir_sign = -1

                duration = self.les_dur_sp.get()


            while bool(self.cpsc.axis_running):

                #wait for the NPSC to stop moving the other axis if it is
                cothread.Sleep(0.05)

            #Take the lock
            self.cpsc.axis_running = True
            
            if "HES" in name:

                # While the axis is not stopped, move in steps of 1

                step_counter = 0
                self.done.set(0)
                # While there are still steps to do, and stop has not been pressed, and the limit switch is not engaged, move
                while step_counter < duration and not bool(self.hes_stop.get()) and (bool(self.pi1.read(cla_hes_limit_pin)) or dir_sign == 1) and (int(self.hes_pos_rb.get()) < int(self.hes_upper_limit.get()) or dir_sign == -1):
                    
                    self._done = 0
                    self.cpsc.ClaHesTime(direction,1)
                    step_counter = step_counter + 1
                    current_position = self.hes_pos_rb.get()
                    self.hes_pos_rb.set(current_position + dir_sign)
                    update_autosave(name.replace("MOVE", "POS-RB"),self.hes_pos_rb.get())

                    print(f"Moving CLA {name} to Pos {direction}, Off 1")


                    t0 = time.perf_counter()
                    t1 = time.perf_counter()

                    # Wait for the move to complete

                    while not bool(self._done) and t1-t0<float(self.timeout):

                        cothread.Sleep(0.05)
                        t1 = time.perf_counter()
                
                self.done.set(1)

            if "LES" in name:

                # While the axis is not stopped, move in steps of 1

                step_counter = 0
                self.done.set(0)
                # While there are still steps to do, and stop has not been pressed, and the limit switch is not engaged, move
                while step_counter < duration and not bool(self.les_stop.get()) and (bool(self.pi1.read(cla_les_limit_pin)) or dir_sign == 1) and (int(self.les_pos_rb.get()) < int(self.les_upper_limit.get()) or dir_sign == -1):

                    self._done = 0
                    self.cpsc.ClaLesTime(direction,1)
                    step_counter = step_counter + 1
                    current_position = self.les_pos_rb.get()
                    self.les_pos_rb.set(current_position + dir_sign)
                    update_autosave(name.replace("MOVE", "POS-RB"),self.les_pos_rb.get())

                    print(f"Moving CLA {name} to Pos {direction}, Off 1")


                    t0 = time.perf_counter()
                    t1 = time.perf_counter()

                    # Wait for the move to complete

                    while not bool(self._done) and t1-t0<float(self.timeout):

                        cothread.Sleep(0.05)
                        t1 = time.perf_counter()

                self.done.set(1)


            #once complete this function will complete
            print("CLA moved finished")
            self.cpsc.axis_running = False

class CPSC_M1():

    """
    CPSC module for moving the Mirrors
    cpsc : Cpsc connection
    """

    def __init__(self, cpsc, timeout=10, **kwargs):
        self.cpsc = cpsc
        self.timeout = timeout
       
        # create the PV's

        self.m11_dir_sp = builder.boolOut('CLA:M11:DIR-SP',ZNAM="UP",ONAM="DOWN",initial_value=0)
        self.m11_dur_sp = builder.longOut('CLA:M11:DUR-SP',initial_value=1,DRVL=1, DRVH=20, EGU="s")
        self.m11_move = builder.boolOut('CLA:M11:MOVE', initial_value=0, always_update=True,on_update_name= lambda v,name: self.move_axis(v,name))
        self.m11_pos_rb = builder.longOut('CLA:M11:POS-RB', initial_value=0, EGU="s")

        #note reversed
        self.m12_dir_sp = builder.boolOut('CLA:M12:DIR-SP',ONAM="UP",ZNAM="DOWN",initial_value=0)
        self.m12_dur_sp = builder.longOut('CLA:M12:DUR-SP',initial_value=1,DRVL=1, DRVH=20, EGU="s")
        self.m12_move = builder.boolOut('CLA:M12:MOVE', initial_value=0,always_update=True, on_update_name= lambda v,name: self.move_axis(v,name))
        self.m12_pos_rb = builder.longOut('CLA:M12:POS-RB', initial_value=0,EGU="s")


        self.m13_dir_sp = builder.boolOut('CLA:M13:DIR-SP',ZNAM="UP",ONAM="DOWN",initial_value=0)
        self.m13_dur_sp = builder.longOut('CLA:M13:DUR-SP',initial_value=1,DRVL=1, DRVH=20, EGU="s")
        self.m13_move = builder.boolOut('CLA:M13:MOVE', initial_value=0,always_update=True, on_update_name= lambda v,name: self.move_axis(v,name))
        self.m13_pos_rb = builder.longOut('CLA:M13:POS-RB', initial_value=0,EGU="s")


        self.done = builder.longIn('CLA:M1:DONE')
        self.error = builder.longIn('CLA:M1:STATUS')
        
        cothread.Spawn(self.update)

    def update(self):

        while True:
          
            response = self.cpsc.ClaM11Status().rstrip()

            #print(f"CLA M1 response = {response}")  
            #print(self.cpsc.axis_running)                   
            tokenList = response.split(",")                
            self.done.set(int(tokenList[0]))
            self.error.set(int(tokenList[1].lower(),16))

            cothread.Sleep(1)



    def move_axis(self,v,name):

        if v == 1:

            if "M11" in name:

                if self.m11_dir_sp.get() == 0:

                    direction = "UP"
                    dir_sign = 1
                
                elif self.m11_dir_sp.get() == 1:

                    direction = "DOWN"
                    dir_sign = -1

                duration = self.m11_dur_sp.get()
                

                

            #Note direction reveresed
            elif "M12" in name:

                if self.m12_dir_sp.get() == 0:

                    direction = "DOWN"
                    dir_sign = -1
                
                elif self.m12_dir_sp.get() == 1:

                    direction = "UP"
                    dir_sign = 1

                duration = self.m12_dur_sp.get()

            elif "M13" in name:

                if self.m13_dir_sp.get() == 0:

                    direction = "UP"
                    dir_sign = 1
                
                elif self.m13_dir_sp.get() == 1:

                    direction = "DOWN"
                    dir_sign = -1

                duration = self.m13_dur_sp.get()


            while bool(self.cpsc.axis_running):

                #wait for the NPSC to stop moving the other axis if it is
                cothread.Sleep(1)

            #Take the lock
            self.cpsc.axis_running = True
            
            if "M11" in name:
                self.cpsc.ClaM11Time(direction,duration)
                current_position = self.m11_pos_rb.get()
                self.m11_pos_rb.set(current_position + dir_sign*duration)
                update_autosave(name.replace("MOVE", "POS-RB"),self.m11_pos_rb.get())
            
            elif "M12" in name:
                self.cpsc.ClaM12Time(direction,duration)
                current_position = self.m12_pos_rb.get()
                self.m12_pos_rb.set(current_position + dir_sign*duration)
                update_autosave(name.replace("MOVE", "POS-RB"),self.m12_pos_rb.get())
            
            elif "M13" in name:
                self.cpsc.ClaM13Time(direction,duration)
                current_position = self.m13_pos_rb.get()
                self.m13_pos_rb.set(current_position + dir_sign*duration)
                update_autosave(name.replace("MOVE", "POS-RB"),self.m13_pos_rb.get())

            print(f"Moving CLA {name} to Pos {direction}, Off {duration}")

            
            t0 = time.perf_counter()
            t1 = time.perf_counter()

            print(f"CLA t0 = {t0}, t1={t1}, timeout = {float(self.timeout)}")
            self.done.set(0)

            while not bool(self.done.get()) and t1-t0<float(self.timeout):

                cothread.Sleep(1) 
                t1 = time.perf_counter()
                print(f"CLA moving done = {self.done.get()} t0 = {t0}, t1={t1}")

            #once complete this function will complete
            print("CLA M1 moved finished")
            self.cpsc.axis_running = False

hes_names = ["HES 0","HES 0","580eV (Empty)","920eV", "HES 0"]
les_names = ["LES 0","54eV","152eV (Empty)","HEM", "92eV"]
hes = NPSC_AXIS(Npsc,"HES", hes_names, timeout=90)
les = NPSC_AXIS(Npsc,"LES", les_names, timeout=90)
cla_es = CPSC_ES(Cpsc, timeout = 25)
cla_m1 = CPSC_M1(Cpsc, timeout = 25)


# Boilerplate get the IOC started
builder.LoadDatabase()
softioc.iocInit()

# Restore autosave positions
for pv_name in autosave:

    print(f"writing {autosave[pv_name]} to {pv_name}")
    caput(pv_name, autosave[pv_name])

# Finally leave the IOC running with an interactive shell.
softioc.interactive_ioc(globals())
