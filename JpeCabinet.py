# File name:        JpeCabinet.py
# Author:           JPE (Robin Drossaert)
# Python version:   3.8

# This file contains methods to communicate with a JPE electronics cabinet.

# 3rd party imports
import serial

# Class representing a JPE electronics cabinet
class JpeCabinet:

    # Private COM settings
    __BAUDRATE = 1250000
    __DATABITS = 8
    __PARITY = serial.PARITY_NONE
    __STOPBITS = serial.STOPBITS_ONE
    __FLOWCONTROL = False
    __TIMEOUT = 5

    # Class constructor
    def __init__(self,comPort):
        self.serialPort = serial.Serial(port = comPort,
                                        baudrate = self.__BAUDRATE,
                                        bytesize = self.__DATABITS,
                                        parity = self.__PARITY,
                                        stopbits = self.__STOPBITS,
                                        timeout = self.__TIMEOUT,
                                        xonxoff = self.__FLOWCONTROL)
        self.serialPort.flushInput()
        self.axis_running = False

    # Class destructor
    def __del__(self):
        self.serialPort.close()

    # Write to COM port
    def _ComWriteRead(self,tx):
        self.serialPort.write(tx.encode('ascii')) # Sent message as ASCII string
        rx =  self.serialPort.read_until(b'\r') # Read until carriage return
        return rx.decode() # Convert message to Python3 string
