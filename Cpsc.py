# File name:        Cpsc.py
# Author:           JPE (Robin Drossaert)
# Python version:   3.8

# This file contains methods to communicate with the CPSC and the CADM in
# it. These include all CLA related serial commands.

# JPE imports
import JpeCabinet

# Class representing the CPSC containing the CADM for the HiTS's CLA
class Cpsc(JpeCabinet.JpeCabinet):

    # Command: CLA_LES [Direction]
    def ClaLes(self,direction):
        command = "CLA_LES " + direction + '\r'
        super()._ComWriteRead(command)

    # Command: CLA_LES_TIME [Direction] [Time]
    def ClaLesTime(self,direction,time):
        command = "CLA_LES_TIME " + direction + ' ' + str(time) + '\r'
        super()._ComWriteRead(command)

    # Command: CLA_LES?
    def ClaLesStatus(self):
        command = "CLA_LES?\r"
        return(super()._ComWriteRead(command))

    # Command: CLA_HES [Direction]
    def ClaHes(self,direction):
        command = "CLA_HES " + direction + '\r'
        super()._ComWriteRead(command)

    # Command: CLA_HES_TIME [Direction] [Time]
    def ClaHesTime(self,direction,time):
        command = "CLA_HES_TIME " + direction + ' ' + str(time) + '\r'
        super()._ComWriteRead(command)

    # Command: CLA_HES?
    def ClaHesStatus(self):
        command = "CLA_HES?\r"
        return(super()._ComWriteRead(command))

    # Command: CLA_M1_1 [Direction]
    def ClaM11(self,direction):
        command = "CLA_M1_1 " + direction + '\r'
        super()._ComWriteRead(command)

    # Command: CLA_M1_1_TIME [Direction] [Time]
    def ClaM11Time(self,direction,time):
        command = "CLA_M1_1_TIME " + direction + ' ' + str(time) + '\r'
        super()._ComWriteRead(command)

    # Command: CLA_M1_1?
    def ClaM11Status(self):
        command = "CLA_M1_1?\r"
        return(super()._ComWriteRead(command))

    # Command: CLA_M1_2 [Direction]
    def ClaM12(self,direction):
        command = "CLA_M1_2 " + direction + '\r'
        super()._ComWriteRead(command)

    # Command: CLA_M1_2_TIME [Direction] [Time]
    def ClaM12Time(self,direction,time):
        command = "CLA_M1_2_TIME " + direction + ' ' + str(time) + '\r'
        super()._ComWriteRead(command)

    # Command: CLA_M1_2?
    def ClaM12Status(self):
        command = "CLA_M1_2?\r"
        return(super()._ComWriteRead(command))

    # Command: CLA_M1_3 [Direction]
    def ClaM13(self,direction):
        command = "CLA_M1_3 " + direction + '\r'
        super()._ComWriteRead(command)

    # Command: CLA_M1_3_TIME [Direction] [Time]
    def ClaM13Time(self,direction,time):
        command = "CLA_M1_3_TIME " + direction + ' ' + str(time) + '\r'
        super()._ComWriteRead(command)

    # Command: CLA_M1_3?
    def ClaM13Status(self):
        command = "CLA_M1_3?\r"
        return(super()._ComWriteRead(command))
