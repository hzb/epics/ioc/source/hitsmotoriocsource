# File name:        Npsc.py
# Author:           JPE (Robin Drossaert)
# Python version:   3.8

# This file contains methods to communicate with the Npsc and the NCM in
# it. These include all NSAU related serial commands.

# 3rd party imports
import time

# JPE imports
import JpeCabinet

# Class representing the NPSC containing the NCM for the HiTS's NSAU
class Npsc(JpeCabinet.JpeCabinet):

    # Waits for the NSAU to stop moving
    def WaitForNcmFinished(self,stage):
        stopped = False
        while(not stopped):
            time.sleep(10) # Wait 10 seconds between each status request
            if(stage == "LES"):
                response = self.LesStatus()
            elif(stage == "HES"):
                response = self.HesStatus()
            tokenList = response.split(",")
            stopped = bool(int(tokenList[2])) # Stopped token, string->int->bool

    # Command: LES [Position]
    def Les(self,position):
        command = "LES " + str(position) + '\r'
        super()._ComWriteRead(command)

    # Command: LES_OFF [Position] [Offset]
    def LesOff(self,position,offset):
        command = "LES_OFF " + str(position) + ' ' + str(offset) + '\r'
        super()._ComWriteRead(command)

    # Command: LES?
    def LesStatus(self):
        command = "LES?\r"
        return(super()._ComWriteRead(command))

    # Command: HES [Position]
    def Hes(self,position):
        command = "HES " + str(position) + '\r'
        super()._ComWriteRead(command)

    # Command: HES_OFF [Position] [Offset]
    def HesOff(self,position,offset):
        command = "HES_OFF " + str(position) + ' ' + str(offset) + '\r'
        super()._ComWriteRead(command)

    # Command: HES?
    def HesStatus(self):
        command = "HES?\r"
        return(super()._ComWriteRead(command))
